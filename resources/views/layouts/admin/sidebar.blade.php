<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{asset('dist/img/logo.png')}}" alt="admin Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ auth()->user()->photo ? asset('images/users/'.auth()->user()->photo) : asset('dist/img/user.png')}}"
                     class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <router-link to="/dashboard" class="nav-link">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            داشبورد
                        </p>
                    </router-link>
                </li>

                @can('isAdmin')
                    <li class="nav-item has-treeview">
                        <!-- menu-open -->
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog"></i>
                            <p>
                                مدیریت
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <router-link to='users' class="nav-link">
                                    <i class="fa fa-users nav-icon"></i>
                                    <p> کاربران </p>
                                </router-link>
                            </li>
                            <li class="nav-item">
                                <router-link to='categories' class="nav-link">
                                    <i class="fa fa-list nav-icon"></i>
                                    <p>دسته بندی ها</p>
                                </router-link>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <router-link to="/developer" class="nav-link">
                            <i class="nav-icon fa fa-code"></i>
                            <p>
                                توسعه دهنده
                            </p>
                        </router-link>
                    </li>
                @endcan
                <li class="nav-item">
                    <router-link to="/profile" class="nav-link">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            پروفایل
                        </p>
                    </router-link>
                </li>
                <li class="nav-item">

                    <a class="nav-link" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-power-off text-danger"></i>
                        <p>
                            {{ __('خروج') }}
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>