<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>AdminPanel | Babak</title>
    <link rel="stylesheet" type="text/css" href="{{asset('/css/app.css')}}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">
    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')

    @yield('content')

    @include('layouts.admin.rightSidebar')
    @include('layouts.admin.footer')
</div>
<!-- ./wrapper -->

<!-- for using gate.js for frontend ACL - this caches user for Gate constructor -->
@auth
    <script>
        window.user = @json(auth()->user());
    </script>
@endauth

<script type="text/javascript" src="{{asset('/js/app.js')}}"></script>
</body>
</html>
