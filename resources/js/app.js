require("./bootstrap");
require("admin-lte");
window.Vue = require("vue");
import Vuex from "vuex";
import VueRouter from "vue-router";
import {routes} from "./routes";

import Gate from './Gate';

Vue.prototype.$gate = new Gate(window.user); //for accessing $gate in application

/*BootstrapVue*/
import BootstrapVue from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);

/*vForm*/
import {Form, HasError, AlertError} from "vform";

/*VueProgressBar*/
import vueprogressbar from 'vue-progressbar';

Vue.use(vueprogressbar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px'
});

/*SweetAlert*/
import Swal from 'sweetalert2';

window.Swal = Swal;
window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

//Vue Plugins
Vue.use(VueRouter);
const router = new VueRouter({routes, mode: "history"});
Vue.use(Vuex);

//variables
window.form = Form;
window.Fire = new Vue();

//Components
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component('pagination', require('laravel-vue-pagination'));


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'not-found',
    require('./components/notFound404').default
);

const app = new Vue({
    el: "#app",
    router,
    data: {
        search: ''
    },
    methods: {
        /* _.debounce is lodash for 1s delay live search*/
        searchIt: _.debounce(() => {
            Fire.$emit('searching');
        }, 1000)
    }
});