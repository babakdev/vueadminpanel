const Dashboard = resolve => {
	require.ensure(['./components/admin/Dashboard.vue'],() => {
		resolve(require('./components/admin/Dashboard.vue'));
	});
};
const Users = resolve => {
	require.ensure(['./components/admin/Users/Users.vue'],() => {
		resolve(require('./components/admin/Users/Users.vue'));
	});
};
const Categories = resolve => {
	require.ensure(['./components/admin/Categories/Categories.vue'],() => {
		resolve(require('./components/admin/Categories/Categories.vue'));
	});
};
const Developer= resolve => {
	require.ensure(['./components/admin/Developer.vue'],() => {
		resolve(require('./components/admin/Developer.vue'));
	});
};
const Profile = resolve => {
	require.ensure(['./components/admin/Profile.vue'],() => {
		resolve(require('./components/admin/Profile.vue'));
	});
};

export const routes = [
	{ path: "/dashboard", component: Dashboard , name: 'dashboard'},
	{ path: "/users", component: Users , name: 'users'},
	{ path: "/categories", component: Categories , name: 'categories'},
	{ path: "/developer", component: Developer , name: 'developer'},
	{ path: "/profile", component: Profile , name: 'profile'},
	{ path: "*", redirect: { name: 'dashboard' } },
];