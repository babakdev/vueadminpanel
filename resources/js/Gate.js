export default class Gate {
    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type == '1';
    }
    isAdminOrAuthor(){
        if(this.user.type == '1' || this.user.type == '2'){
            return true;
        }
    }
    isUser(){
        return this.user.type == '4';
    }
}