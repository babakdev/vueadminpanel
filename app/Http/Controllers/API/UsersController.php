<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('isAdmin') || Gate::allows('isAuthor')) {
            return User::latest()->paginate(3);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');
        $this->validate($request, [
            'name' => 'required | string | max:191',
            'email' => 'email | max:191 | unique:users',
            'phone' => 'required | numeric | digits:11 | unique:users,phone',
            'password' => 'required | string | min:6',
            'type' => 'required',
        ]);
        $data = $request->all();
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'status' => $data['status'],
            'type' => $data['type'],
            'photo' => $data['photo'] ?? null,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');

        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required | string | max:191',
            /*.$user->id says that skip this user for unique email or phone*/
            'email' => 'email | max:191 | unique:users,email,' . $user->id,
            'phone' => 'required | numeric | digits:11 | unique:users,phone,' . $user->id,
            'password' => 'sometimes | string | min:6',
            'type' => 'required',
        ]);
        $user->update($request->all());

        return ['message' => 'User has been updated'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        User::findOrFail($id)->delete();
        return ['message' => 'User has been deleted'];
    }

    public function profile()
    {
        return auth('api')->user();
    }

    public function updateProfile()
    {
        $user = auth('api')->user();

        $this->validate(request(), [
            'name' => 'required | string | max:191',
            'email' => 'sometimes | email | max:191 | unique:users,email,' . $user->id,
            'password' => 'sometimes | required | string | min:6',
        ]);

        /*upload photo*/
        $currentPhoto = $user->photo;
        if (request('photo') != $currentPhoto) {

            /*take extention of file*/
            $name = time() . '.'
                . explode('/',
                    explode(':',
                        substr(request('photo'), 0,
                            strpos(request('photo'), ';')))[1])[1];
            /*! take extention of file*/

            \Image::make(request('photo'))->save(public_path('images/users/') . $name);

            request()->merge(['photo' => $name]);
        }

        $oldUserPhoto = public_path('images/users/') . $currentPhoto;
        if (file_exists($oldUserPhoto)) {
            unlink($oldUserPhoto);
        }

        if (!empty(request('password'))) {
            request()->merge(['password' => Hash::make(request('password'))]);
        }

        $user->update(request()->all());
        return ['message' => 'User has been updated'];
    }

    public function search()
    {
        if ($search = request('q')) {
            $users = User::where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%")
                    ->orWhere('phone', 'LIKE', "%$search%");
            })->paginate();
        }else{
            $users = User::latest()->paginate(3);
        }
        return $users;
    }
}
